#!/usr/bin/env python3
import os
from distutils.sysconfig import get_config_var

from setuptools import setup, Extension

#  https://stackoverflow.com/questions/4529555/building-a-ctypes-based-c-library-with-distutils
from setuptools._vendor import six
from setuptools.command.build_ext import build_ext as build_ext_orig, _build_ext, get_abi3_suffix, use_stubs, libtype
from setuptools.extension import Library


class CTypesExtension(Extension):
    pass

class build_ext(build_ext_orig):


    def get_ext_filename(self, fullname):
        return fullname+".so"


setup(
    name="nfc_list_wrapper",
    version="0.1.1",
    py_modules = ["nfc_list_wrapper.get_tag"],
    ext_modules=[
        CTypesExtension(
            name="nfc_list_wrapper.nfc_get_uid",
            sources=["nfc_list_wrapper/nfc-get-uid.c"],
            libraries=['nfc'],
            include_dirs = ['/usr/local/include'],
        ),
    ],
    cmdclass={'build_ext': build_ext},
)
