le super package python `nfcpy` (et aussi `pynfc`) ne marchent pas avec mon ACS ACR122U
# Installation
Il faut installer la librairie ``libnfc``
```
sudo apt install libnfc
pip install nfc-list-wrapper --extra-index-url https://gitlab.com/api/v4/projects/26145408/packages/pypi/simple
```
Si sudo apt install libnfc ne marche pas, je vous propose de suivre l'installation proposé ici : https://github.com/nfc-tools/libnfc
## Drivers ACS ACR122U
Il faut désactiver les drivers du projet linux-nfc (qui ont l'air cool mais rien d'utilisable facilement)
```
modprobe -r pn533_usb pn533 nfc
```
## Permissions utilisateurs
Il faut se donner les droits pour utiliser directement le lecteur USB
```
# echo 'SUBSYSTEMS=="usb", ATTRS{idVendor}=="072f", ATTRS{idProduct}=="2200", SYMLINK+="sub20", GROUP="plugdev", MODE="660"' > /etc/udev/rules.d/99-libnfc.rules
usermod -aG plugdev $USER
```
## Build le package python
```
./setup.py build
./setup.py sdist
./setup.py bdist_wheel # nécessite d'installer le packe 'wheel'
```
l'archive apparaît dans ``dist/nfc_list_wrapper-0.0.1.tar.gz

par contre ce n'est pas portable entre les architectures, il faudra le build pour le raspberry pi
## installe le package python
dans un autre projet,
```
pip install <nom_du_fichier>.tar.gz
```
# Utilisation
```
export LD_LIBRARY_PATH=.
gcc -lnfc -o nfc_get_uid.so -shared -fPIC nfc-get-uid.c
python3 nfc_list_c_wrapper.py
```

# Problèmes
L'appel à la fonction C fait probablement des fuites de mémoire.
Askip il faut allouer les objets depuis Python, à voir
