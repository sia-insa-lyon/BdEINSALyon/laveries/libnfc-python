from ctypes import cdll, c_char_p, c_void_p
import pathlib

nfc_lib = cdll.LoadLibrary(str(pathlib.Path(__file__).parent / "nfc_get_uid.so"))

nfc_lib.get_tag_uid.restype = c_char_p  # POINTER(c_int8 * 10)
nfc_lib.py_nfc_init.restype = c_void_p
nfc_lib.py_nfc_close.restype = c_void_p

def init_nfc():
    nfc_lib.py_nfc_init()

def close_nfc():
    nfc_lib.py_nfc_close()

def get_tag_uid() -> str:  # max 10 caractères
    """
    Renvoie le tag scanné sous forme de caractères hexadécimaux
    ex : '61445E02'
    """
    data = nfc_lib.get_tag_uid()
    if len(data) < 2:
        return ""
    lenght = int(data[0])

    uid_hex = ["%02X" % x for x in data[1:lenght + 1]]
    return "".join(uid_hex)
# gcc -lnfc -o nfc_get_uid.so -shared -fPIC nfc-get-uid.c
class NfcScanner(object):
    """
    Context Manager, qui permet d'initialiser/rendre le lecteur NFC
    (avec libnfc, un seul programme peut utiliser le lecteur à la fois)
    """
    def __init__(self):
        pass
    def __enter__(self):
        nfc_lib.py_nfc_init()
        return get_tag_uid
    def __exit__(self, type, value, traceback):
        nfc_lib.py_nfc_close()


if __name__ == '__main__':
    print(get_tag_uid())
