// code honteusement volé de libnfc > nfc-utils > nfc-list.c
#include <err.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <nfc/nfc.h>

#define MAX_DEVICE_COUNT 1
#define MAX_TARGET_COUNT 1
#define ERR(...)  warnx ("ERROR: " __VA_ARGS__ )

static nfc_device *pnd;

nfc_context *context;

nfc_connstring connstrings[MAX_DEVICE_COUNT];
nfc_target ant[MAX_TARGET_COUNT];


void py_nfc_init(){
    printf("Init NFC...\n");
    nfc_init(&context);
    if (context == NULL) {
        ERR("Unable to init libnfc (malloc)");
        exit(EXIT_FAILURE);
    }
    size_t szDeviceFound = nfc_list_devices(context, connstrings, MAX_DEVICE_COUNT);

    if (szDeviceFound == 0) {
        printf("No NFC device found.\n");
    }


        pnd = nfc_open(context, connstrings[0]);

        if (pnd == NULL) {
            ERR("Unable to open NFC device: %s", connstrings[0]);
        }
        if (nfc_initiator_init(pnd) < 0) {
            nfc_perror(pnd, "nfc_initiator_init");
            nfc_exit(context);
            exit(EXIT_FAILURE);
        }

        printf("NFC device: %s opened\n", nfc_device_get_name(pnd));
}

void py_nfc_close(){
    nfc_close(pnd);
    nfc_exit(context);
}
char *get_tag_uid() {
    int res = 0;

    char *uid_returned = malloc(11);
    memset(uid_returned, 0, 11);
        nfc_modulation nm;

        nm.nmt = NMT_ISO14443A; //peut lire les cartes étudiantes paris-saclay (7 octets)
        nm.nbr = NBR_106;
        // List ISO14443A targets
        if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
            if (res > 0) {
                /*printf("OUI\n");
                print_nfc_target(&ant[0], verbose);
                printf("\n");*/

                uid_returned[0] = ant[0].nti.nai.szUidLen;
                memcpy(uid_returned + 1, &ant[0].nti.nai.abtUid, 10);
/*
                printf("%d octets : ",ant[0].nti.nai.szUidLen);
                for (int ix=0;ix<10;ix++){
                    printf("%02x ",ant[0].nti.nai.abtUid[ix]);
                }
                printf("\n");
*/
            }
        }
/*
    if (mask & 0x02) {
      nm.nmt = NMT_FELICA;
      nm.nbr = NBR_212;
      // List Felica tags
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d Felica (212 kbps) passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x04) {
      nm.nmt = NMT_FELICA;
      nm.nbr = NBR_424;
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d Felica (424 kbps) passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x08) {
      nm.nmt = NMT_ISO14443B;
      nm.nbr = NBR_106;
      // List ISO14443B targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443B passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x10) {
      nm.nmt = NMT_ISO14443BI;
      nm.nbr = NBR_106;
      // List ISO14443B' targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443B' passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x20) {
      nm.nmt = NMT_ISO14443B2SR;
      nm.nbr = NBR_106;
      // List ISO14443B-2 ST SRx family targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443B-2 ST SRx passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x40) {
      nm.nmt = NMT_ISO14443B2CT;
      nm.nbr = NBR_106;
      // List ISO14443B-2 ASK CTx family targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443B-2 ASK CTx passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x80) {
      nm.nmt = NMT_ISO14443BICLASS;
      nm.nbr = NBR_106;
      // List ISO14443B iClass targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443B iClass passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x100) {
      nm.nmt = NMT_JEWEL;
      nm.nbr = NBR_106;
      // List Jewel targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443A-3 Jewel passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }

    if (mask & 0x200) {
      nm.nmt = NMT_BARCODE;
      nm.nbr = NBR_106;
      // List NFC Barcode targets
      if ((res = nfc_initiator_list_passive_targets(pnd, nm, ant, MAX_TARGET_COUNT)) >= 0) {
        int n;
        if (verbose || (res > 0)) {
          printf("%d ISO14443A-2 NFC Barcode passive target(s) found%s\n", res, (res == 0) ? ".\n" : ":");
        }
        for (n = 0; n < res; n++) {
          print_nfc_target(&ant[n], verbose);
          printf("\n");
        }
      }
    }
    */


    return uid_returned;
}


int main(int argc, const char *argv[]) {

    printf("%s\n",get_tag_uid());
    exit(EXIT_SUCCESS);
}
