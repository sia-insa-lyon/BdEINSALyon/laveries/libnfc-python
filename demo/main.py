#!/usr/bin/env python3
from nfc_list_wrapper.get_tag import get_tag_uid, init_nfc, close_nfc, NfcScanner

if __name__=='__main__':

    with NfcScanner() as s:
        print("tag",s())
        print("tag",s())
        print("tag",s())



    init_nfc()

    tag = get_tag_uid()
    print("tag",tag)
    assert tag != "", "no tag found"
    bcc = int(tag[0],base=16) ^ int(tag[1],base=16) ^ int(tag[2],base=16) ^ int(tag[3],base=16)
    print("BCC:",hex(bcc))

    close_nfc()
